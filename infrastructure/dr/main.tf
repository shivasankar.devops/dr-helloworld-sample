provider "aws" {
  region = var.aws_region
}

resource "aws_vpc" "dr_vpc" {
  cidr_block = var.vpc_cidr_block
}

resource "aws_subnet" "dr_subnet" {
  count            = var.subnets_count
  cidr_block       = cidrsubnet(var.vpc_cidr_block, 8, count.index)
  availability_zone = element(data.aws_availability_zones.available.names, count.index)
  vpc_id           = aws_vpc.dr_vpc.id
}

resource "aws_security_group" "dr_sg" {
  name_prefix = "dr-sg-"
}

resource "aws_lb" "dr_app_lb" {
  name               = "dr-app-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups   = [aws_security_group.dr_sg.id]
  subnets            = aws_subnet.dr_subnet[*].id
}

# Define AWS ECS resources, task definitions, and ECS service for DR environment
# You can reuse the task definition and service configurations similar to the main environment

resource "aws_ecs_task_definition" "dr_hello_world_task" {
  # Define your DR task definition here, reusing the same container definition as the main environment
}

resource "aws_ecs_service" "dr_hello_world_service" {
  # Define your DR ECS service here, reusing the same service configuration as the
