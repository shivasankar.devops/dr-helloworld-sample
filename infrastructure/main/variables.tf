variable "aws_region" {
  description = "The AWS region in which to deploy resources."
  type        = string
  default     = "us-east-1"
}

variable "vpc_cidr_block" {
  description = "CIDR block for the VPC."
  type        = string
  default     = "10.0.0.0/16"
}

variable "subnets_count" {
  description = "Number of subnets to create."
  type        = number
  default     = 2
}

variable "container_image_url" {
  description = "URL of the Docker container image for the 'Hello World' application."
  type        = string
}

variable "container_port" {
  description = "Port on which the 'Hello World' application container listens."
  type        = number
  default     = 8080
}
